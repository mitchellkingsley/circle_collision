import pygame
import random
import math
class Circle:
    def __init__(self, screen, color):
        self.screen = screen
        self.color = color
        self.xpos = random.randint(50, 950)
        self.ypos = random.randint(50, 950)
        self.xvel = 0.1
        self.yvel = 0.1
        self.draw_circle()

    def update_pos(self):
        self.xpos = self.xpos + self.xvel
        self.ypos = self.ypos + self.yvel
        self.draw_circle()

    def draw_circle(self):
        pygame.draw.circle(self.screen, self.color, (int (self.xpos), int(self.ypos)), 50)

    def check_wall_collision(self):
        if self.xpos > 950 or self.xpos < 50:
            self.xvel = -self.xvel
        if self.ypos > 950 or self.ypos < 50:
            self.yvel = -self.yvel

    def check_ball_collisions(self, ball_list):
        for ball in ball_list:
            if self is ball:
                continue 
            if math.sqrt((self.xpos - ball.xpos) ** 2 + (self.ypos - ball.ypos) ** 2) < 100:
                self_x, self_y, ball_x, ball_y = self.reflection(ball)

                # Set new self vectors.
                self.xvel = self.xvel + self_x
                self.yvel = self.yvel + self_y

                # Set new ball vectors.
                ball.xvel = ball.xvel + ball_x
                ball.yvel = ball.yvel + ball_y

    def reflection(self, ball):
        xmid = (self.xpos + ball.xpos) / 2 
        ymid = (self.ypos + ball.ypos) / 2

        return (self.xpos - xmid) / 50, (self.ypos - ymid) / 50, (ball.xpos - xmid) / 50, (ball.ypos - ymid) / 50

    def friction(self):
        if self.xvel > 0:
            self.xvel -= 0.0001
        
        if self.xvel < 0:
            self.xvel += 0.0001
        
        if self.yvel > 0:
            self.yvel -= 0.0001

        if self.yvel < 0:
            self.yvel += 0.0001

def main():
    pygame.init()
    pygame.display.set_caption("Circle Collision")
    screen = pygame.display.set_mode((1000,1000))


    # define a variable to control the main loop
    running = True

    # Define Colors
    white = (255, 255, 255)
    black = (0, 0, 0)
    blue  = (150, 150, 255)
    red   = (255, 100, 100)

    # Draw Circles
    circle1 = Circle(screen, black)
    circle2 = Circle(screen, blue)
    circle3 = Circle(screen, red)
   
    # Create Ball List
    ball_list = [circle1, circle2, circle3]

    # main loop
    while running:
        # event handling, gets all event from the event queue
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                circle1.xvel = random.randint(0,5)
                circle1.yvel = random.randint(0,5)
            # only do something if the event is of type QUIT
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                running = False

        screen.fill(white)

        for circle in ball_list:
            circle.update_pos()
            circle.friction()
            circle.check_wall_collision()
            circle.check_ball_collisions(ball_list)
            circle.draw_circle()

        pygame.display.update()
main()

